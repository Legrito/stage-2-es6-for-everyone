import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
if(fighter) {
  const { source, name, health, attack, defense} = fighter;
  let healthWidth = 500 / 100 * health;
  let attackWidth = 500 / 100 * attack;
  let defenseWidth = 500 / 100 * defense;
  fighterElement.innerHTML = 
  `<img src="${source}">
  <h2>${name}</h2>
  <ul>
  <li>HEALTH: ${health}
  <div style="width: ${healthWidth}px; height: 8px;"></div>
  </li>
  <li>ATTACK: ${attack}
  <div style="width: ${attackWidth}px; height: 8px;"></div>
  </li>
  <li>DEFENSE: ${defense}
  <div style="width: ${defenseWidth}px; height: 8px;"></div>
  </li>
  </ul>`;
 }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
