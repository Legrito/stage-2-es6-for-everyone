import { createElement } from '../../helpers/domHelper';

export function showModal({ title, bodyElement, health, attack, defense, onClose = () => {} }) {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, health, attack, defense, onClose }); 
  
  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, health, attack, defense, onClose }) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);
  const fighterHealth = createElement({ tagName: 'div', className: 'modal-health' });
  const fighterAttack = createElement({ tagName: 'div', className: 'modal-attack' });
  const fighterDefense = createElement({ tagName: 'div', className: 'modal-defense' });

  fighterHealth.innerText = health;
  fighterAttack.innerText = attack;
  fighterDefense.innerText = defense;
  modalContainer.append(header, fighterHealth, fighterAttack, fighterDefense, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title, onClose) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
