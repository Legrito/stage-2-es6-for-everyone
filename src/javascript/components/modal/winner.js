import { showModal } from '../modal/modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const imageElement = createFighterImage(fighter);
  const modalElement = {
    title: `The winner is <<${fighter.name}>>`,
    bodyElement: imageElement,
    health: `Health <<${fighter.health}>>`,
    attack: `Attack <<${fighter.attack}>>`,
    defense: `Defense <<${fighter.defense}>>`,
    onClose: () => {
      location.reload();
    }
  };
  showModal(modalElement);
  // call showModal function 
}
